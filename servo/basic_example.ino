#include <Servo.h> 


Servo servo;


void setup() { 
     servo.attach(9);
} 
 
void loop() {
     int back = random(10,20);
     int fwd  = random(40,50);

     servo.write(back);
     delay(2000);

     servo.write(fwd);
     delay(2000);
}
